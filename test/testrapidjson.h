#ifndef JANNA_TEST_RAPIDJSON_H_
#define JANNA_TEST_RAPIDJSON_H_
class TestRapidJson {
public:
    TestRapidJson()  = default;
    ~TestRapidJson() = default;

public:
    bool testReadJson();
    void testRapidJsonAll();
};

#endif

#include <QtTest>
#include <memory>

#include "testrapidjson.h"
class TestMain : public QObject {
    Q_OBJECT
public:
    TestMain() {
        m_rapid_json = std::make_unique<TestRapidJson>();
    }
private slots:
    void testCommon() {
        this->m_rapid_json->testRapidJsonAll();
    }
    void testRapidJson() {
        this->m_rapid_json->testRapidJsonAll();
    }

private:
    std::unique_ptr<TestRapidJson> m_rapid_json;
};

QTEST_MAIN(TestMain)
#include "testmain.moc"
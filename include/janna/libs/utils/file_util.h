#ifndef JANNA_LIBS_UTILS_FILE_UTIL_H_
#define JANNA_LIBS_UTILS_FILE_UTIL_H_

#include <QObject>
namespace janna {
class FileUtil : public QObject {
    Q_OBJECT
public:
    FileUtil()           = default;
    ~FileUtil() override = default;

public:
    /**
     * @brief 读取一个文本文件的内容
     * 
     * @param filePath 文件路径
     * @return QString 文件内容. 如果文件不存在则返回空字符串
    */
    static QString readFileContentAsQString(const QString& filePath);
};
};  // namespace janna

#endif
#ifndef JANNA_LIBS_UTILS_PUBSUB_H_
#define JANNA_LIBS_UTILS_PUBSUB_H_

#include <spdlog/logger.h>

#include <QMap>
#include <QObject>
#include <QString>
#include <QVariant>

namespace janna {
/**
 * @brief 消息主题
 */
typedef struct {
    /**
    * @brief 主题来源
    */
    QString source;
    /**
     * @brief 主题
     */
    QString topic;
    /**
     * @brief 处理消息后需要发布的消息
     */
    QString publish;
} JannaTopic;

class Pubsub : public QObject {
    Q_OBJECT

public:
    static Pubsub* getInstance();

    Pubsub() = default;

private:
    std::shared_ptr<spdlog::logger> log;

public:
    void publish(const JannaTopic& topic, const QMap<QString, QVariant>& param);

signals:
    void signalPublish(const JannaTopic& topic, const QMap<QString, QVariant>& param);
};
};  // namespace janna
#endif

#include "janna/libs/utils/pubsub.h"

#include <fmt/core.h>

#include <QMutex>
#include <cstdlib>

namespace janna {
Q_GLOBAL_STATIC(Pubsub, pubsub)

Pubsub* Pubsub::getInstance() {
    return pubsub();
}

void Pubsub::publish(const JannaTopic& topic, const QMap<QString, QVariant>& param) {
    emit signalPublish(topic, param);
}
};  // namespace janna
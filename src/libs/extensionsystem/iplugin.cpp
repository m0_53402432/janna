#include "janna/libs/extensionsystem/iplugin.h"

namespace janna {
QString PluginMetadata::id() const {
    return this->m_id;
}
void PluginMetadata::id(QString&& id) {
    this->m_id = std::move(id);
}

QString PluginMetadata::version() const {
    return this->m_version;
}
void PluginMetadata::version(QString&& version) {
    this->m_version = std::move(version);
}

QString PluginMetadata::author() const {
    return this->m_author;
}
void PluginMetadata::author(QString&& author) {
    this->m_author = std::move(author);
}

QString PluginMetadata::name() const {
    return this->m_name;
}
void PluginMetadata::name(QString&& name) {
    this->m_name = std::move(name);
}

QList<PluginMetadataSubscribe*> PluginMetadata::subscribes() const {
    return this->m_subscribes;
}

QString PluginMetadataSubscribe::source() const {
    return this->m_source;
}
void PluginMetadataSubscribe::source(QString&& source) {
    this->m_source = std::move(source);
}

QString PluginMetadataSubscribe::topic() const {
    return this->m_topic;
}
void PluginMetadataSubscribe::topic(QString&& topic) {
    this->m_topic = std::move(topic);
}

QString PluginMetadataSubscribe::publish() const {
    return this->m_publish;
}
void PluginMetadataSubscribe::publish(QString&& publish) {
    this->m_publish = std::move(publish);
}

void JannaPluginInterface::initPoints(QList<JannaTopic>&& points) {
    this->m_points = std::move(points);
}

void JannaPluginInterface::initPluginMetadata(PluginMetadata* metadata) {
    this->m_plugin_metadta = metadata;
}

void JannaPluginInterface::pointHandlerInterceptor(const JannaTopic& topic, const QMap<QString, QVariant>& param) {
    if (this->m_points.isEmpty()) {
        return;
    }
    foreach (auto t, this->m_points) {
        if (t.source == topic.source && t.topic == topic.topic) {
            // 响应消息
            auto publishParam = handler(topic, param);
            // 发布消息
            if (!t.publish.isEmpty()) {
                JannaTopic publishTopic;
                publishTopic.source = author() + "." + pluginName();
                publishTopic.topic  = t.publish;
                Pubsub::getInstance()->publish(publishTopic, publishParam);
            }
            break;
        }
    }
}

};  // namespace janna
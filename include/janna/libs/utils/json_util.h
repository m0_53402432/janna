#ifndef JANNA_LIBS_UTILS_JSON_UTIL_H_
#define JANNA_LIBS_UTILS_JSON_UTIL_H_

#include <rapidjson/document.h>
#include <rapidjson/pointer.h>
#include <rapidjson/prettywriter.h>
#include <rapidjson/schema.h>
#include <rapidjson/stringbuffer.h>
#include <rapidjson/writer.h>

#include <QObject>
#include <memory>
namespace janna {
class JsonUtil : public QObject {
    Q_OBJECT
public:
    JsonUtil()           = default;
    ~JsonUtil() override = default;

public:
    static std::unique_ptr<rapidjson::Document> readJsonAsDocument(const QString &jsonFilePath);
    static QString                              readJsonAsQstring(const QString &jsonFilePath, bool pretty = false);
    static bool                                 validateJson(const QString &jsonFilePath, const QString &schemaFilePath);
};
};  // namespace janna

#endif
aux_source_directory(. JANNA_LIBS_UTILS_SOURCES)
list(APPEND JANNA_LIBS_UTILS_SOURCES
    ${CMAKE_SOURCE_DIR}/include/janna/libs/utils/app_config.h
    ${CMAKE_SOURCE_DIR}/include/janna/libs/utils/configuration.h
    ${CMAKE_SOURCE_DIR}/include/janna/libs/utils/file_util.h
    ${CMAKE_SOURCE_DIR}/include/janna/libs/utils/json_util.h
    ${CMAKE_SOURCE_DIR}/include/janna/libs/utils/log_util.h
    ${CMAKE_SOURCE_DIR}/include/janna/libs/utils/pubsub.h
)
qt_add_library(${janna_libs_utils_lib_name} SHARED ${JANNA_LIBS_UTILS_SOURCES})

target_link_libraries(${janna_libs_utils_lib_name} PUBLIC
    fmt::fmt
    spdlog::spdlog
    Qt${QT_VERSION_MAJOR}::Core
)
set_target_properties(${janna_libs_utils_lib_name} PROPERTIES LIBRARY_OUTPUT_DIRECTORY ${lib_dir})

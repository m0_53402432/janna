#ifndef JANNA_LIBS_UTILS_CONFIGURATION_H_
#define JANNA_LIBS_UTILS_CONFIGURATION_H_
namespace janna {
// clang-format off
#define JANNA_VERSION_MAJOR 1
#define JANNA_VERSION_MINOR 0
#define JANNA_VERSION_PATCH 0
// clang-format on
};  // namespace janna
#endif

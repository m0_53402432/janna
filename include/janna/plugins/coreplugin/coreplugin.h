#ifndef JANNA_PLUGINS_CORE_PLUGIN_CORE_PLUGIN_H_
#define JANNA_PLUGINS_CORE_PLUGIN_CORE_PLUGIN_H_

#include <QObject>
#include <QString>
#include <QtPlugin>

#include "janna/libs/extensionsystem/iplugin.h"

namespace janna {

class CorePlugin : public QObject, JannaPluginInterface {
    Q_OBJECT
    Q_PLUGIN_METADATA(IID "com.laolang.janna.JannaPluginInterface" FILE "janna.core.json")
    Q_INTERFACES(janna::JannaPluginInterface)

public:
    CorePlugin();
    ~CorePlugin() override = default;
    QString                 pluginName() override;
    QString                 pluginVersion() override;
    QString                 author() override;
    QMap<QString, QVariant> handler(const JannaTopic& topic, const QMap<QString, QVariant>& param) override;
    bool                    initialize(QString* error_message) override;

private:
    std::shared_ptr<spdlog::logger> log;
};

};  // namespace janna

#endif
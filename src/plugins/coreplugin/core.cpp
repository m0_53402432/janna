#include "janna/plugins/coreplugin/core.h"

#include <QMenuBar>
#include <QMessageBox>

#include "janna/libs/utils/log_util.h"
#include "janna/libs/utils/pubsub.h"

namespace janna {

namespace core {

namespace internal {

MainWindow::MainWindow(QWidget *parent) : QMainWindow(parent) {
    this->log = LogUtil::getLogger("mainwindow");

    this->resize(800, 600);
    this->setWindowTitle("janna");

    init_menu();

    createGUI();
}

void MainWindow::init_menu() {
    QMenuBar *menubar = this->menuBar();
    this->setMenuBar(menubar);
    this->m_menu_file     = menubar->addMenu("文件");
    this->m_menu_edit     = menubar->addMenu("编辑");
    this->m_menu_select   = menubar->addMenu("选择");
    this->m_menu_view     = menubar->addMenu("查看");
    this->m_menu_goto     = menubar->addMenu("转到");
    this->m_menu_run      = menubar->addMenu("运行");
    this->m_menu_terminal = menubar->addMenu("终端");
    this->m_menu_help     = menubar->addMenu("帮助");

    /* 文件菜单 */
    this->m_act_file_new = this->m_menu_file->addAction("新建");
    this->m_act_file_new->setIcon(QPixmap(":/icons/newfile_wiz"));

    /* 帮助菜单 */
    this->m_act_help_welcome          = this->m_menu_help->addAction("欢迎");
    this->m_act_help_show_all_command = this->m_menu_help->addAction("显示所有命令");
    this->m_act_help_document         = this->m_menu_help->addAction("文档");
    this->m_menu_help->addSeparator();
    this->m_act_help_tips = this->m_menu_help->addAction("贴士和技巧");
    this->m_menu_help->addSeparator();
    this->m_act_help_about = this->m_menu_help->addAction("关于");
    this->m_act_help_about->setIcon(QPixmap(":/logo/logo_16"));
    this->m_act_help_about_qt = this->m_menu_help->addAction("关于 Qt");

    connect(this->m_act_help_about_qt, &QAction::triggered, [&]() {
        // SPDLOG_LOGGER_INFO(this->log, fmt::format("janna"));
        QMessageBox::aboutQt(nullptr);
    });
}

void MainWindow::sendEcho() {
    JannaTopic topic;
    topic.source = "janna";
    topic.topic  = lineEdit->text();
    QMap<QString, QVariant> param;
    param.insert("msg", "hello janna");
    Pubsub::getInstance()->publish(topic, param);
}

void MainWindow::createGUI() {
    auto *centerWidget = new QWidget;
    this->setCentralWidget(centerWidget);

    lineEdit = new QLineEdit;
    button   = new QPushButton(tr("Send Message"));

    connect(button, &QPushButton::clicked, this, &MainWindow::sendEcho);

    layout = new QGridLayout;
    layout->addWidget(new QLabel(tr("Message:")), 0, 0);
    layout->addWidget(lineEdit, 0, 1);
    layout->addWidget(button, 2, 1, Qt::AlignRight);
    layout->setSizeConstraint(QLayout::SetFixedSize);
    centerWidget->setLayout(layout);
}
};  // namespace internal

Q_GLOBAL_STATIC(janna::core::Core, core)
Core *getInstance() {
    return core();
}
Core::Core() {
    this->log = LogUtil::getLogger("coreplugin");
}

internal::MainWindow *Core::mainwindow() const {
    return this->m_mainwindow;
}

void Core::initMainWindow() {
    this->m_mainwindow = new internal::MainWindow();
}

};  // namespace core

};  // namespace janna

extern "C" {
janna::core::Core *janna_core_core_getInstance() {
    return janna::core::getInstance();
}
}
#ifndef JANNA_LIBS_UTILS_APP_CONFIG_H_
#define JANNA_LIBS_UTILS_APP_CONFIG_H_

#include <qdir.h>
#include <spdlog/spdlog.h>

#include <QCoreApplication>
#include <QDir>
#include <QObject>
#include <QString>

#include "janna/libs/utils/configuration.h"

namespace janna {
#define JANNA_VERSION_MAJOR_COPY (JANNA_VERSION_MAJOR)
#define JANNA_VERSION_MINOR_COPY (JANNA_VERSION_MINOR)
#define JANNA_VERSION_PATCH_COPY (JANNA_VERSION_PATCH)

#define JANNA_VERSION_STR(R) #R
#define JANNA_VERSION_STR2(R) JANNA_VERSION_STR(R)

class AppConfig : public QObject {
    Q_OBJECT

public:
    static AppConfig* getInstance();

    AppConfig();

public:
    QString appVersion() {
        return this->m_app_version;
    }
    [[nodiscard]] int appVersionMajor() const {
        return this->m_app_version_major;
    }
    [[nodiscard]] int appVersionMinor() const {
        return this->m_app_version_major;
    }
    [[nodiscard]] int appVersionPatch() const {
        return this->m_app_version_major;
    }
    QDir appDir() {
        return this->m_app_dir;
    }

    QDir dataDir() {
        return this->m_dataDir;
    }
    void dataDir(const QString& dataDir) {
        this->m_dataDir = QDir(dataDir);
    }

    QDir pluginsAppDir() {
        return this->m_plugins_app_dir;
    }

    QDir pluginsUserDir() {
        return this->m_dataDir.absolutePath() + "/plugins";
    }

    QString pluginsAppJsonPath() {
        return this->m_plugins_app_json_path;
    }

    QString pluginListJsonSchemaPath() {
        QDir path = this->m_app_dir;
        path.cdUp();
        return path.absolutePath() + "/res/plugins/plugins.schema.json";
    }

    QString pluginDescJsonSchemaPath() {
        QDir path = this->m_app_dir;
        path.cdUp();
        return path.absolutePath() + "/res/plugins/plugin_desc.schema.json";
    }

    spdlog::level::level_enum logLevel() {
        return this->m_log_level;
    }

private:
    // app 版本信息
    QString m_app_version;
    int     m_app_version_major;
    int     m_app_version_minor;
    int     m_app_version_patch;
    /**
     * @brief 应用程序所在目录
     */
    QDir m_app_dir;
    /**
     * @brief 用户数据目录
     */
    QDir m_dataDir;
    /**
     * @brief 应用插件目录
     */
    QDir    m_plugins_app_dir;
    QString m_plugins_app_json_path;
    QString m_plugin_app_json_schema_path;
    /**
     * @brief 日志级别
     */
    spdlog::level::level_enum m_log_level;
};
};  // namespace janna
#endif

#include "janna/libs/utils/file_util.h"

#include <QFile>

namespace janna {
QString FileUtil::readFileContentAsQString(const QString& filePath) {
    QFile file(filePath);
    if (!file.exists()) {
        return {};
    }
    if (!file.open(QIODevice::ReadOnly | QIODevice::Text)) {
        return {};
    }
    return file.readAll();
}
};  // namespace janna
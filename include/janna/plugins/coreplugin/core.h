#ifndef JANNA_PLUGINS_CORE_PLUGIN_CORE_H_
#define JANNA_PLUGINS_CORE_PLUGIN_CORE_H_

#include <spdlog/logger.h>

#include <QAction>
#include <QGridLayout>
#include <QLabel>
#include <QLineEdit>
#include <QList>
#include <QMainWindow>
#include <QMap>
#include <QMenu>
#include <QObject>
#include <QPushButton>
#include <QString>
#include <QVariant>

namespace janna {

namespace core {

namespace internal {
class MainWindow : public QMainWindow {
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = nullptr);
    ~MainWindow() override = default;

private:
    void init_menu();

private:
    QMenu *m_menu_file{nullptr};
    QMenu *m_menu_edit{nullptr};
    QMenu *m_menu_select{nullptr};
    QMenu *m_menu_view{nullptr};
    QMenu *m_menu_goto{nullptr};
    QMenu *m_menu_run{nullptr};
    QMenu *m_menu_terminal{nullptr};
    QMenu *m_menu_help{nullptr};

    QAction *m_act_file_new{nullptr};

    QAction                        *m_act_help_welcome{nullptr};
    QAction                        *m_act_help_show_all_command{nullptr};
    QAction                        *m_act_help_document{nullptr};
    QAction                        *m_act_help_tips{nullptr};
    QAction                        *m_act_help_about{nullptr};
    QAction                        *m_act_help_about_qt{nullptr};
    std::shared_ptr<spdlog::logger> log;

private slots:
    void sendEcho();

private:
    void         createGUI();
    QLineEdit   *lineEdit{nullptr};
    QPushButton *button{nullptr};
    QGridLayout *layout{nullptr};
};
};  // namespace internal

class Core : public QObject {
    Q_OBJECT

public:
    static Core *getInstance();
    Core();

public:
    [[nodiscard]] internal::MainWindow *mainwindow() const;
    void                                initMainWindow();

private:
    std::shared_ptr<spdlog::logger> log;
    internal::MainWindow           *m_mainwindow{nullptr};
};
};  // namespace core

};  // namespace janna

extern "C" {
janna::core::Core *janna_core_core_getInstance();
}

#endif
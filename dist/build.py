#!/usr/bin/env python3
# -*- coding: UTF-8 -*-
import subprocess
import sys
import shutil

# app 名称
APP_NAME = "janna"
# 家目录
HOME_DIR = "/home/laolang"
# 打包目录
DEPLOY_PATH = "/home/laolang/tmp/janna/janna"
# 编译目录
BUILD_PATH = "/home/laolang/csdn/gitcode/janna/build/ninja-release"
# qt platforms 目录
QT_PLATFORMS_PATH = "/home/laolang/program/qt6/6.5.3/gcc_64/plugins/platforms"
# 自己的动态库或其他三方库
LIBS = ("lib_janna_libs_extensionsystem", "lib_janna_libs_utils", "libspdlog", "libfmt")
# 构建目录中的可执行程序
BUILD_BIN_PATH = f"{BUILD_PATH}/dist/bin/janna"
# 应用程序插件
PLUGINS_APP = ["janna.corep-1.0.0"]
########################################################################################################################

# 当前脚本所在目录
SCRIPT_DIR = sys.path[0]
# 打包 qt 动态库的脚本
PACK_SHELL = "pack.sh"
# 复制 libqxcb.so 依赖库的脚本
LDD_LIBQXCB_SHELL = "ldd_qxcb.sh"


# 发布目录中的可执行程序
DEPLOY_BIN_PATH = f"{DEPLOY_PATH}/bin/{APP_NAME}"


def print_blank():
    for i in range(0, 1):
        print("")


def init_deploy():
    print("清理并初始化打包目录")
    # 创建目录
    subprocess.call(f"rm -rf {DEPLOY_PATH}", shell=True)
    subprocess.call(f"mkdir -p {DEPLOY_PATH}/bin", shell=True)
    subprocess.call(f"mkdir -p {DEPLOY_PATH}/data", shell=True)
    subprocess.call(f"mkdir -p {DEPLOY_PATH}/lib", shell=True)
    subprocess.call(f"mkdir -p {DEPLOY_PATH}/logs", shell=True)
    subprocess.call(f"mkdir -p {DEPLOY_PATH}/plugins", shell=True)
    subprocess.call(f"mkdir -p {DEPLOY_PATH}/qt_lib", shell=True)
    subprocess.call(f"mkdir -p {DEPLOY_PATH}/sbin", shell=True)

    # 复制应用程序文件
    shutil.copyfile(f"{BUILD_BIN_PATH}", f"{DEPLOY_BIN_PATH}")
    subprocess.call(f"chmod +x {DEPLOY_BIN_PATH}", shell=True)
    shutil.copyfile(f"{SCRIPT_DIR}/{APP_NAME}", f"{DEPLOY_PATH}/sbin/{APP_NAME}")
    subprocess.call(f"chmod +x {DEPLOY_PATH}/sbin/{APP_NAME}", shell=True)
    subprocess.call(f"cp -r {BUILD_PATH}/dist/res {DEPLOY_PATH}/res", shell=True)


def copy_qt_lib():
    print("复制 qt 动态库")
    pack_shell_path = f"{DEPLOY_PATH}/qt_lib/{PACK_SHELL}"
    # 复制脚本
    shutil.copyfile(f"{SCRIPT_DIR}/{PACK_SHELL}", pack_shell_path)

    # 修改脚本
    pack_shell_file_data = ""
    with open(pack_shell_path, "r", encoding="utf-8") as f:
        for line in f:
            if line.startswith("des="):
                line = f"des={DEPLOY_PATH}/qt_lib\n"
            elif line.startswith("exe="):
                line = f"exe={APP_NAME}\n"
            pack_shell_file_data += line
    with open(pack_shell_path, "w", encoding="utf-8") as f:
        f.write(pack_shell_file_data)

    # 复制应用程序文件
    shutil.copyfile(f"{BUILD_BIN_PATH}", f"{DEPLOY_PATH}/qt_lib/{APP_NAME}")
    subprocess.call(f"cd {DEPLOY_PATH}/qt_lib && sh {PACK_SHELL}", shell=True)

    # 复制 qt 动态库
    subprocess.call(f"cd {DEPLOY_PATH}/qt_lib && sh {PACK_SHELL}", shell=True)

    # 删除应用程序和脚本
    subprocess.call(f"rm {DEPLOY_PATH}/qt_lib/{PACK_SHELL}", shell=True)
    subprocess.call(f"rm {DEPLOY_PATH}/qt_lib/{APP_NAME}", shell=True)


def copy_qt_plugin():
    print("复制 qt plugin")
    # 清理 qt platforms 目录
    subprocess.call(f"rm -rf {QT_PLATFORMS_PATH}/{LDD_LIBQXCB_SHELL}", shell=True)
    subprocess.call(f"rm -rf {QT_PLATFORMS_PATH}/lib", shell=True)
    # 复制 qt platforms 下的文件
    shutil.copytree(QT_PLATFORMS_PATH, f"{DEPLOY_PATH}/qt_lib/plugins/platforms")
    subprocess.call(f"rm {DEPLOY_PATH}/qt_lib/plugins/platforms/*.debug", shell=True)

    # 复制 ldd_qxcb.sh 脚本
    shutil.copyfile(
        f"{SCRIPT_DIR}/{LDD_LIBQXCB_SHELL}", f"{QT_PLATFORMS_PATH}/{LDD_LIBQXCB_SHELL}"
    )
    # 执行 ldd_qxcb.sh 脚本
    subprocess.call(f"cd {QT_PLATFORMS_PATH} && sh {LDD_LIBQXCB_SHELL}", shell=True)
    # 复制 libqxcb.so 依赖库
    shutil.copytree(f"{QT_PLATFORMS_PATH}/lib", f"{DEPLOY_PATH}/qt_lib/lib")


def copy_lib():
    print("复制自定义库和三方库")
    for lib in LIBS:
        subprocess.call(
            f"mv {DEPLOY_PATH}/qt_lib/{lib}* {DEPLOY_PATH}/lib/", shell=True
        )


def copy_desktop():
    print("复制快捷方式")
    # 复制图标
    shutil.copyfile(f"{SCRIPT_DIR}/../res/logo.xpm", f"{DEPLOY_PATH}/logo.xpm")
    desktop_file_path = f"{HOME_DIR}/.local/share/applications/{APP_NAME}.desktop"
    # 删除快捷方式
    subprocess.call(f"rm -rf {desktop_file_path}", shell=True)
    # 复制快捷方式
    subprocess.call(
        f"cp {SCRIPT_DIR}/{APP_NAME}.desktop {desktop_file_path}", shell=True
    )
    subprocess.call(f"chmod +x {desktop_file_path}", shell=True)

    # 修改脚本
    desktop_file_data = ""
    with open(
        f"{HOME_DIR}/.local/share/applications/{APP_NAME}.desktop",
        "r",
        encoding="utf-8",
    ) as f:
        for line in f:
            if line.startswith("Exec="):
                line = f'Exec="{DEPLOY_PATH}/sbin/{APP_NAME}" %f\n'
            elif line.startswith("Path="):
                line = f"Path={DEPLOY_PATH}/sbin\n"
            elif line.startswith("Icon="):
                line = f"Icon={DEPLOY_PATH}/logo.xpm\n"
            desktop_file_data += line
    with open(
        f"{HOME_DIR}/.local/share/applications/{APP_NAME}.desktop",
        "w",
        encoding="utf-8",
    ) as f:
        f.write(desktop_file_data)


def copy_app_plugin():
    print("复制应用插件")
    subprocess.call(
        f"cp {BUILD_PATH}/dist/plugins/plugins.json {DEPLOY_PATH}/plugins/plugins.json",
        shell=True,
    )
    for plugin in PLUGINS_APP:
        subprocess.call(
            f"cp -rf {BUILD_PATH}/dist/plugins/{plugin} {DEPLOY_PATH}/plugins/{plugin}",
            shell=True,
        )
        subprocess.call(
            f"rm -rf {DEPLOY_PATH}/plugins/{plugin}/CMakeFiles",
            shell=True,
        )
        subprocess.call(
            f"rm -rf {DEPLOY_PATH}/plugins/{plugin}/*_autogen",
            shell=True,
        )
        subprocess.call(
            f"rm -rf {DEPLOY_PATH}/plugins/{plugin}/*.cmake",
            shell=True,
        )


def tar_file():
    subprocess.call(
        f"cd {DEPLOY_PATH} && cd .. && tar -I 'xz -9' -cf {APP_NAME}.tar.xz {APP_NAME}",
        shell=True,
    )


if __name__ == "__main__":
    print("开始打包...")
    print_blank()

    init_deploy()
    print_blank()

    copy_qt_lib()
    print_blank()

    copy_qt_plugin()
    print_blank()

    copy_lib()
    print_blank()

    copy_desktop()
    print_blank()

    copy_app_plugin()
    print_blank()

    # tar_file()
    # print_blank()
    # print("打包完成...")

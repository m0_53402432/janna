#include "janna/libs/utils/json_util.h"

#include <rapidjson/document.h>
#include <rapidjson/prettywriter.h>
#include <rapidjson/schema.h>
#include <rapidjson/stringbuffer.h>
#include <rapidjson/writer.h>

#include "janna/libs/utils/file_util.h"

namespace janna {

std::unique_ptr<rapidjson::Document> JsonUtil::readJsonAsDocument(const QString &jsonFilePath) {
    auto jsonQString = FileUtil::readFileContentAsQString(jsonFilePath);
    if (jsonQString.isEmpty()) {
        return nullptr;
    }
    std::unique_ptr<rapidjson::Document> doc = std::make_unique<rapidjson::Document>();
    doc->Parse(jsonQString.toStdString().c_str());
    if (doc->HasParseError()) {
        doc.reset();
    }
    return doc;
}
QString JsonUtil::readJsonAsQstring(const QString &jsonFilePath, bool pretty) {
    auto doc = JsonUtil::readJsonAsDocument(jsonFilePath);
    if (!doc) {
        return "";
    }
    rapidjson::StringBuffer buffer;
    if (pretty) {
        rapidjson::PrettyWriter<rapidjson::StringBuffer> writer(buffer);
        doc->Accept(writer);
    } else {
        rapidjson::Writer<rapidjson::StringBuffer> writer(buffer);
        doc->Accept(writer);
    }
    return buffer.GetString();
}
bool JsonUtil::validateJson(const QString &jsonFilePath, const QString &schemaFilePath) {
    auto schemaDoc = JsonUtil::readJsonAsDocument(schemaFilePath);
    auto jsonDoc   = JsonUtil::readJsonAsDocument(jsonFilePath);
    if (!schemaDoc || !jsonDoc) {
        return false;
    }

    rapidjson::SchemaDocument  schema(*schemaDoc);
    rapidjson::SchemaValidator validator(schema);
    return jsonDoc->Accept(validator);
}
};  // namespace janna
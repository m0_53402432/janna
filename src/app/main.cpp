
#include <QApplication>
#include <QCommandLineParser>
#include <QFile>
#include <QScreen>
#include <iostream>

#include "janna/libs/extensionsystem/plugin_manager.h"
#include "janna/libs/utils/app_config.h"
#include "janna/libs/utils/log_util.h"

/**
 * @brief 打印程序配置信息
 */
void printAppConfig() {
    std::shared_ptr<spdlog::logger> log       = janna::LogUtil::getLogger("app");
    auto                            appConfig = janna::AppConfig::getInstance();
    SPDLOG_LOGGER_DEBUG(log, fmt::format("appConfig[appVersion]:{}", appConfig->appVersion().toStdString()));
    SPDLOG_LOGGER_DEBUG(log, fmt::format("appConfig[appDir]:{}", appConfig->appDir().absolutePath().toStdString()));
    SPDLOG_LOGGER_DEBUG(log, fmt::format("appConfig[dataDir]:{}", appConfig->dataDir().absolutePath().toStdString()));
    SPDLOG_LOGGER_DEBUG(log, fmt::format("appConfig[pluginsAppDir]:{}", appConfig->pluginsAppDir().absolutePath().toStdString()));
    SPDLOG_LOGGER_DEBUG(log, fmt::format("appConfig[logLevel]:{}", static_cast<int>(appConfig->logLevel())));
}

/**
 * @brief 初始化命令行参数
 * 
 * @param app 应用程序实例
 */
void initCommandLine(QApplication &app) {
    std::shared_ptr<spdlog::logger> log = janna::LogUtil::getLogger("app");

    QCommandLineParser parser;
    // 创建命令行选项
    QCommandLineOption dataDirOption("data-dir", "用户目录", "<dir>", "");
    parser.addOption(dataDirOption);

    // 解析命令行参数
    parser.process(app);

    // 获取命令行参数值
    QString dataDir = parser.value(dataDirOption);
    if (!dataDir.isEmpty()) {
        janna::AppConfig::getInstance()->dataDir(dataDir);
    }
    SPDLOG_LOGGER_DEBUG(log, fmt::format("dataDir:{}", janna::AppConfig::getInstance()->dataDir().absolutePath().toStdString()));
    SPDLOG_LOGGER_DEBUG(log, fmt::format("appDir:{}", janna::AppConfig::getInstance()->appDir().absolutePath().toStdString()));
}

int main(int argc, char *argv[]) {
    QApplication a(argc, argv);
    janna::LogUtil::init(janna::AppConfig::getInstance()->logLevel(), "../logs/app.log");
    std::shared_ptr<spdlog::logger> log = janna::LogUtil::getLogger("app");
    SPDLOG_LOGGER_DEBUG(log, fmt::format("janna is running..."));

    initCommandLine(a);
    printAppConfig();

    // 加载程序自带插件
    janna::PluginManager::getInstance()->loadPlugins(janna::AppConfig::getInstance()->pluginsAppDir().absolutePath());

    return QApplication::exec();
}
